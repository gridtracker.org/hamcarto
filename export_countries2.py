import psycopg2
import json

# PostgreSQL connection parameters
db_params = {
    'host': 'localhost',
    'database': 'gis',
    'user': 'renderer',
    'password': 'renderer'
}

try:
    # Connect to the PostgreSQL database
    connection = psycopg2.connect(**db_params)

    # Create a cursor object
    cursor = connection.cursor()

    # Query to retrieve all country names and their English names
    query = """
    SELECT name, tags->'name:en' AS name_en
    FROM planet_osm_polygon
    WHERE boundary = 'administrative' AND admin_level = '2'
    """

    # Execute the query
    cursor.execute(query)

    # Fetch all results as dictionaries with 'name' as the key and 'name_en' as the value
    country_data = [{'name': row[0], 'name_en': row[1]} for row in cursor.fetchall()]

    # Close the cursor and the database connection
    cursor.close()
    connection.close()

    # Write the country data to a JSON file
    with open('country_names.json', 'w') as json_file:
        json.dump(country_data, json_file)

    print("Country names with English names successfully written to country_names.json")

except Exception as e:
    print(f"An error occurred: {str(e)}")

