const { Worker, isMainThread, parentPort, workerData } = require('worker_threads');
const axios = require('axios');
const minimist = require('minimist');

const args = minimist(process.argv.slice(2));
const concurrencyLimit = args.concurrency || 4;

if (isMainThread) {
    mainThread();
} else {
    processTile(workerData.z, workerData.x, workerData.y);
}

function mainThread() {
    const baseUrl = 'http:/localhost/tile/{z}/{x}/{y}.png';
    const maxZoom = 20;
    const outDir = 'gs://maps.gridtracker.org/gt-carto/{z}/{y}';
    const outFile = '/{x}.png';

    const totalTiles = 4 ** maxZoom;
    let completedTiles = 0;
    let failedTiles = 0;

    function updateScreen(z, x, y) {
        process.stdout.clearLine();
        process.stdout.cursorTo(0);
        const percent = (completedTiles + failedTiles) / totalTiles * 100;
        process.stdout.write(`Progress: ${completedTiles} completed, ${failedTiles} failed, ${percent.toFixed(2)}%`);
        process.stdout.write(`  Processing z=${z} x=${x} y=${y}`);
    }

    async function startWorker(z, x, y) {
        return new Promise(async (resolve, reject) => {
            let processing = true;

            const worker = new Worker(__filename, {
                workerData: { z, x, y },
            });

            worker.on('message', (message) => {
                if (message.success) {
                    completedTiles++;
                } else {
                    failedTiles++;
                }

                processing = false;
                updateScreen(z, x, y);
                resolve();
            });

            // Set a timeout for the worker thread to prevent hanging
            setTimeout(() => {
                if (processing) {
                    worker.terminate(); // Terminate the worker if it's still processing after a timeout
                    failedTiles++;
                    updateScreen(z, x, y);
                    resolve();
                }
            }, 10000); // 10 seconds timeout
        });
    }

    async function processTiles() {
        for (let z = 0; z < maxZoom; z++) {
            for (let x = 0; x < 2 ** z; x++) {
                for (let y = 0; y < 2 ** z; y++) {
                    await startWorker(z, x, y);
                }
            }
        }

        console.log('\nCompleted tile check.');
    }

    process.stdout.write(`Starting tile check...`);
    processTiles();
}

function processTile(z, x, y) {
    const baseUrl = 'http:/localhost/tile/{z}/{x}/{y}.png';
    const getUrl = baseUrl
        .replace('{z}', z)
        .replace('{x}', x)
        .replace('{y}', y);

    axios
        .get(getUrl, { timeout: 10000 })
        .then((response) => {
            parentPort.postMessage({ success: response.status === 200 });
        })
        .catch((error) => {
            parentPort.postMessage({ success: false });
        });
}

