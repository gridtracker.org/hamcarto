#!/bin/bash

echo "$(date -u +'%Y-%m-%d %H:%M:%S')  Tile server started." | tee -a process.log

docker run \
    --privileged \
    -e THREADS=24 \
    -e "OSM2PGSQL_EXTRA_ARGS=-C 81920" \
    -e "OSM2PGSQL_EXTRA_ARGS=-v" \
    -e REPLICATION_URL=https://planet.openstreetmap.org/replication/minute/ \
    -e MAX_INTERVAL_SECONDS=60 \
    -e UPDATES=enabled \
    -p 80:80 \
    -p 5432:5432 \
    -v /home/osm-mappers/pgsql:/data/database/ \
    --shm-size="10g" \
    -v /home/osm-mappers/osm-tiles:/data/tiles/ \
    -d overv/openstreetmap-tile-server \
    run &

