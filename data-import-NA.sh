#!/bin/bash

echo "$(date -u +'%Y-%m-%d %H:%M:%S')  Launching OSM data import process." | tee -a process.log

#-e "OSM2PGSQL_EXTRA_ARGS=-C 81920" \
#-e SLIM=disabled \
docker run \
	--privileged \
	-e THREADS=24 \
	-e "OSM2PGSQL_EXTRA_ARGS=-C 81920" \
	--memory=112640M \
	--memory-swap=112640M \
	-e FLAT_NODES=enabled \
	-v /home/osm-mappers/north-america-latest.osm.pbf:/data/region.osm.pbf \
	-v /home/osm-mappers/pgsql:/data/database/ \
	-p 8080:8080 \
	-p 5432:5432 \
	overv/openstreetmap-tile-server \
	import

echo "$(date -u +'%Y-%m-%d %H:%M:%S')  Completed OSM data import process." | tee -a process.log

