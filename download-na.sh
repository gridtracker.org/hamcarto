#!/bin/bash

echo "$(date -u +'%Y-%m-%d %H:%M:%S')  Starting download of north-america-latest.osm.pbf." | tee -a process.log

aria2c -c -x 10 -s 10 -d ~ https://download.geofabrik.de/north-america-latest.osm.pbf

mv north-america-*.osm.pbf north-america.osm.pbf

echo "$(date -u +'%Y-%m-%d %H:%M:%S')  Completed download of north-america-latest.osm.pbf." | tee -a process.log
