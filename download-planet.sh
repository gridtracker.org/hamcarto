#!/bin/bash

echo "$(date -u +'%Y-%m-%d %H:%M:%S')  Starting download of planet-latest.osm.pbf." | tee -a process.log

aria2c -c -x 10 -s 10 -d ~ https://planet.openstreetmap.org/pbf/planet-latest.osm.pbf https://planet.osm.org/pbf/planet-latest.osm.pbf https://download.bbbike.org/osm/planet/planet-latest.osm.pbf https://ftp.fau.de/osm-planet/pbf/planet-latest.osm.pbf

mv planet-*.osm.pbf planet.osm.pbf

echo "$(date -u +'%Y-%m-%d %H:%M:%S')  Completed download of plaent-latest.osm.pbf." | tee -a process.log
