import psycopg2
import json

# Database connection parameters
db_params = {
    'host': 'localhost',
    'database': 'gis',
    'user': 'renderer',
    'password': 'renderer'
}

try:
    # Connect to the PostgreSQL database
    connection = psycopg2.connect(**db_params)

    # Create a cursor object
    cursor = connection.cursor()

    # Read the data from the JSON file
    with open('countries.json', 'r') as json_file:
        country_data = json.load(json_file)

    # Update the OSM database with the new tags
    for country in country_data:
        name = country['name']
        itu = country['itu']

        # Construct and execute the SQL update statement
        update_query = """
        UPDATE planet_osm_polygon
        SET tags = tags || hstore(%s, %s)
        WHERE name = %s AND boundary = 'administrative' AND admin_level = '2'
        """

        cursor.execute(update_query, (itu, 'yes', name))
        connection.commit()

    # Close the cursor and the database connection
    cursor.close()
    connection.close()

    print("Tags successfully updated in the OSM PostgreSQL database.")

except Exception as e:
    print(f"An error occurred: {str(e)}")

