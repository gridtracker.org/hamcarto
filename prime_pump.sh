#!/bin/bash

# This script grabs map tiles off a local OSM server
# for OpenLayers and saves them to a Google Storage Bucket.
# Copyright 2022 Jon Angliss KF5QHQ and Matthew Chambers NR0Q
# The authors make not claim to the suitability of this script
# for any purposes outside of our internal use.
# If you use it and break something, don't blame us!

baseUrl="http:/localhost/tile/{z}/{x}/{y}.png"
maxZoom=20
outDir="gs://maps.gridtracker.org/gt-carto/{z}/{y}"
outFile="/{x}.png"

echo "$(date -u +'%Y-%m-%d %H:%M:%S')  Starting tile check." | tee -a process.log
tiles=0
code200=0
code404=0

z=0
while [ $z -lt $maxZoom ]
do
    x=0
    while [ $x -lt $((2 ** $z)) ]
    do
	y=0
        while [ $y -lt $((2 ** $z)) ]
        do
            getUrl=${baseUrl/{z\}/$z}
            getUrl=${getUrl/{y\}/$y}
            getUrl=${getUrl/{x\}/$x}

            putDir=${outDir/{z\}/$z}
            putDir=${putDir/{x\}/$x}
            putDir=${putDir/{y\}/$y}

            putFile=${putDir}${outFile}
            putFile=${putFile/{z\}/$z}
            putFile=${putFile/{x\}/$x}
            putFile=${putFile/{y\}/$y}

            #echo "Processing ${getUrl}"
	    tiles=$(($tiles + 1))
	    code=$(curl -m 15 -o /dev/null -s -Iw '%{http_code}' $getUrl)
	    if [[ "$code" =~ "200" ]]; then
		    code200=$(($code200 + 1))
	    else
		    code404=$(($code404 + 1))
		    echo "Failed z=${z} x=${x} y=${y}" >> osm-fail.log
	    fi
	    clear
	    echo "z=${z} x=${x} y=${y}"
	    echo "${tiles} tiles evaluated."
	    echo "${code200} tiles rendered."
	    echo "${code404} tiles failed."
	    percent=`bc <<< "scale=2; $code200/$tiles*100"`
	    echo "${percent}% tiles completed"
            ((y++))
        done
        ((x++))
    done
    ((z++))
done

echo "$(date -u +'%Y-%m-%d %H:%M:%S')  Completed tile check." | tee -a process.log
