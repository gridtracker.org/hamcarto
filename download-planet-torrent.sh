#!/bin/bash

echo "$(date -u +'%Y-%m-%d %H:%M:%S')  Starting download of planet-latest.osm.pbf." | tee -a process.log

aria2c -c -j 25 --bt-external-ip=172.13.41.217 --dht-listen-port=6909 --listen-port=6999 -d ~ https://planet.osm.org/pbf/planet-latest.osm.pbf.torrent

mv planet-*.osm.pbf planet.osm.pbf

echo "$(date -u +'%Y-%m-%d %H:%M:%S')  Completed download of plaent-latest.osm.pbf." | tee -a process.log
